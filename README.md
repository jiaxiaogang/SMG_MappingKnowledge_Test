# SMG_MappingKnowledge

## 尽量使用第三方;但如果找不到合适的;就自己写


### 开源或第三方知识图谱研究
Knowledge Graph Database:

#### neo4j
	http://www.neo4j.org/

#### Cayley:　google员工开发，非官方的
	https://github.com/google/cayley

#### 百度知识图谱开放平台:
	http://kgopen.baidu.com/data/post

#### 中文开放知识图谱联盟
	http://openkg.cn/

#### CN-DBpedia:复旦大学GDM实验室
	http://kw.fudan.edu.cn/

#### 楚辞:
	http://www.chuci.info/

#### 搜狗人物关系:
	http://www.sogou.com/tupu/person.html

#### graphconnect;
http://graphconnect.com/?ref=home

#### 百度:DuerOS

#### 亚马逊Alexa

#### 跨语言知识图谱 xlore

#### 世界各个组织建立的知识库多达50余种，相关的应用系统更是达到了上百种。
  http://www.36dsj.com/archives/18644?utm_source=tuicool&utm_medium=referral


#### 知识计算引擎——OpenKN

1,规模最大
	概念最多的知识库是Probase
2,facebook
	实体搜索服务Graph Search
3,谷歌的知识图谱KnowledgeGraph
4,Evi公司TrueKnowledge知识搜索平台
5,基于维基百科的DBpedia
6,Wolfram的知识计算平台WolframAlpha等;
7,美国官方政府网站Data.gov, 
8,Evi公司的TrueKnowledge知识搜索平台2、
9,有代表性的知识库或应用系统有KnowItAll, TextRunner, NELL, Probase, Satori,PROSPERA, SOFIE以及一些基于维基百科等在线百科知识构建的知识库DBpedia, YAG,Omega, WikiTaxonomy。



